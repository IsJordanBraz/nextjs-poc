import styles from '../styles/Home.module.css'
import ProductList from '../components/ProductList'
import { getProducts } from '../utils/request';
import { IProduct } from '../utils/types';
import { wrapper } from '../store/store'
import { END } from 'redux-saga'
import { loadOrders } from '../store/actions';
import { useSelector } from 'react-redux'

export default function Home() {
  const products = useSelector((state) => state.orders)

  return (
    <div className={styles.container}>
      <ProductList products={products}/>
    </div>
  )
}

export const getServerSideProps = wrapper.getServerSideProps(async ({store, req, res, ...etc}) => {
    store.dispatch(loadOrders())
    store.dispatch(END)
    await (store as any).sagaTask.toPromise()
  }  
);