import Link from 'next/link'
import OrderList from '../components/OrderList';

import Styles from '../styles/pedidos.module.css'
import { getPedidos } from '../utils/request';

export default function pedidos({ ordersList }) {
  return (
    <div>      
      <div className={Styles.container}>
        <Link href="/">
          <a className={Styles.linkHome}>&lArr; Voltar para home</a>
        </Link>
        <span className={Styles.title}>PEDIDOS ({ordersList.length})</span>
        <OrderList ordersList={ordersList}/>    
      </div >        
    </div>
  )
}

export async function getServerSideProps() {  
  const ordersList = await getPedidos();
  return {
    props: {
      ordersList
    },
  }
}