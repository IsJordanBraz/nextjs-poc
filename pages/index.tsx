import { END } from 'redux-saga'
import { loadOrders } from '../store/actions'
import { wrapper } from '../store/store'

export default function Home() {
  return (
    <div>
      index            
    </div>
  )
}

export const getStaticProps = wrapper.getStaticProps(async ({ store }) => {

  if (!store.getState().orders) {
    store.dispatch(loadOrders())
    store.dispatch(END)
  }
 
  await (store as any).sagaTask.toPromise()
})