import '../styles/globals.css'
import Layout from '../components/Layout'
import Header from '../components/Header'

import { wrapper } from '../store/store'

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Header />
      <Component {...pageProps} />
    </Layout> 
  )  
}

export default wrapper.withRedux(MyApp)
