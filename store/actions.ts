export const actionTypes = {
  LOAD_ORDERS: 'LOAD_ORDERS',
  LOAD_ORDERS_SUCCESS: 'LOAD_ORDERS_SUCCESS',
  LOAD_ORDERS_FAILURE: 'LOAD_ORDERS_FAILURE',
}

export function loadOrders() {
  return { type: actionTypes.LOAD_ORDERS }
}

export function loadOrdersSuccess(data) {
  return {
    type: actionTypes.LOAD_ORDERS_SUCCESS,
    data,
  }
}

export function loadOrdersFailure(error) {
  return {
    type: actionTypes.LOAD_ORDERS_FAILURE,
    error,
  }
}