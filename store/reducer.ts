import { actionTypes } from './actions'
import { HYDRATE } from 'next-redux-wrapper'

const initialState = {
  error: false,
  orders: null,
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case HYDRATE: {
      return { ...state, ...action.payload }
    }
    case actionTypes.LOAD_ORDERS_SUCCESS:
      return {
        ...state,
        ...{ orders: action.data },
    }
    case actionTypes.LOAD_ORDERS_FAILURE:
      return {
        ...state,
        ...{ error: action.error },
    }   
    default:
      return state
  }
}

export default reducer