import { all, call, put, takeLatest } from 'redux-saga/effects'
import { ProductsMock } from '../utils/Mock'
import { getProducts } from '../utils/request';
import { actionTypes, loadOrdersSuccess, loadOrdersFailure } from './actions';
import axios from 'axios'

async function getProductMocks() {
  try {
    const produtos = await axios.request({
      url: 'http://localhost:8000/api/v1/products',
      method: "GET",
      auth: {
        username: 'jordan',
        password: 'jordan'
      },
    })
    return produtos.data
  } catch(err) {
    return ProductsMock
  }  
}

function* loadOrdersSaga() {
  try {
    const res = yield call(getProductMocks)
    yield put(loadOrdersSuccess(res))
  } catch(err) {
    yield put(loadOrdersFailure(err))
  }
}

function* rootSaga() {
  yield all([
    takeLatest(actionTypes.LOAD_ORDERS, loadOrdersSaga),
  ])
}

export default rootSaga