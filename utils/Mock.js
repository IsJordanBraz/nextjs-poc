export const ProductsMock = [
  {
    "id": 3,
    "name": "Mock Fralda Pampers Confort Sec Bag Xg Com 58 Unidades",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119601-15.jpg?ims=480x",
    "price": "69,90"
  },
  {
    "id": 4,
    "name": "Kit Tresemme Tresplex Regeneração Shampoo 400ml + Condicionador 200ml",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119895-15.jpg?ims=480x",
    "price": "20,99"
  },
  {
    "id": 5,
    "name": "Toalhas Umedecidas Panvel Baby 96 Unidades",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/114255-15.jpg?ims=480x",
    "price": "6,99"
  },
  {
    "id": 6,
    "name": "Fralda Pampers Confort Sec Bag Xxg Com 56 Unidades",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119602-15.jpg?ims=480x",
    "price": "69,90"
  },
  {
    "id": 7,
    "name": "Kit Creme Dental Colgate Total 12 Clean Mint 90g Leve 4 Pague 3",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/465880-15.jpg?ims=480x",
    "price": "23,99"
  },
  {
    "id": 8,
    "name": "Lenços Umedecidas Huggies Pure Care Leve 4 Pague 3",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119352-15.jpg?ims=480x",
    "price": "29,90"
  },
  {
    "id": 9,
    "name": "Lenços Umedecidos Huggies One&done Leve 4 Pague 3",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119353-15.jpg?ims=480x",
    "price": "29,90"
  },
  {
    "id": 10,
    "name": "Sabonete Líquido Protex Íntimo Delicate Care 40ml",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119336-15.jpg?ims=480x",
    "price": "4,99"
  },
  {
    "id": 11,
    "name": "Sabonete Líquido Protex Íntimo Delicate Care 40ml",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119336-15.jpg?ims=480x",
    "price": "4,99"
  },
  {
    "id": 12,
    "name": "Sabonete Líquido Protex Íntimo Delicate Care 40ml",
    "image": "https://cdn1.staticpanvel.com.br/produtos/15/119336-15.jpg?ims=480x",
    "price": "4,99"
  }
];

export const PedidosMock = [
  {
    "id": 1,
    "product": {
      "id": 3,
      "name": "Fralda Pampers Confort Sec Bag Xg Com 58 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119601-15.jpg?ims=480x",
      "price": "69,90"
    }
  },
  {
    "id": 2,
    "product": {
      "id": 5,
      "name": "Toalhas Umedecidas Panvel Baby 96 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/114255-15.jpg?ims=480x",
      "price": "6,99"
    }
  },
  {
    "id": 5,
    "product": {
      "id": 9,
      "name": "Lenços Umedecidos Huggies One&done Leve 4 Pague 3",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119353-15.jpg?ims=480x",
      "price": "29,90"
    }
  },
  {
    "id": 6,
    "product": {
      "id": 7,
      "name": "Kit Creme Dental Colgate Total 12 Clean Mint 90g Leve 4 Pague 3",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/465880-15.jpg?ims=480x",
      "price": "23,99"
    }
  },
  {
    "id": 7,
    "product": {
      "id": 4,
      "name": "Kit Tresemme Tresplex Regeneração Shampoo 400ml + Condicionador 200ml",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119895-15.jpg?ims=480x",
      "price": "20,99"
    }
  },
  {
    "id": 8,
    "product": {
      "id": 6,
      "name": "Fralda Pampers Confort Sec Bag Xxg Com 56 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119602-15.jpg?ims=480x",
      "price": "69,90"
    }
  },
  {
    "id": 9,
    "product": {
      "id": 5,
      "name": "Toalhas Umedecidas Panvel Baby 96 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/114255-15.jpg?ims=480x",
      "price": "6,99"
    }
  },
  {
    "id": 10,
    "product": {
      "id": 10,
      "name": "Sabonete Líquido Protex Íntimo Delicate Care 40ml",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119336-15.jpg?ims=480x",
      "price": "4,99"
    }
  },
  {
    "id": 11,
    "product": {
      "id": 10,
      "name": "Sabonete Líquido Protex Íntimo Delicate Care 40ml",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119336-15.jpg?ims=480x",
      "price": "4,99"
    }
  },
  {
    "id": 12,
    "product": {
      "id": 4,
      "name": "Kit Tresemme Tresplex Regeneração Shampoo 400ml + Condicionador 200ml",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119895-15.jpg?ims=480x",
      "price": "20,99"
    }
  },
  {
    "id": 13,
    "product": {
      "id": 3,
      "name": "Fralda Pampers Confort Sec Bag Xg Com 58 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119601-15.jpg?ims=480x",
      "price": "69,90"
    }
  },
  {
    "id": 14,
    "product": {
      "id": 3,
      "name": "Fralda Pampers Confort Sec Bag Xg Com 58 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119601-15.jpg?ims=480x",
      "price": "69,90"
    }
  },
  {
    "id": 15,
    "product": {
      "id": 3,
      "name": "Fralda Pampers Confort Sec Bag Xg Com 58 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119601-15.jpg?ims=480x",
      "price": "69,90"
    }
  },
  {
    "id": 16,
    "product": {
      "id": 4,
      "name": "Kit Tresemme Tresplex Regeneração Shampoo 400ml + Condicionador 200ml",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119895-15.jpg?ims=480x",
      "price": "20,99"
    }
  },
  {
    "id": 17,
    "product": {
      "id": 4,
      "name": "Kit Tresemme Tresplex Regeneração Shampoo 400ml + Condicionador 200ml",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119895-15.jpg?ims=480x",
      "price": "20,99"
    }
  },
  {
    "id": 18,
    "product": {
      "id": 8,
      "name": "Lenços Umedecidas Huggies Pure Care Leve 4 Pague 3",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/119352-15.jpg?ims=480x",
      "price": "29,90"
    }
  },
  {
    "id": 19,
    "product": {
      "id": 5,
      "name": "Toalhas Umedecidas Panvel Baby 96 Unidades",
      "image": "https://cdn1.staticpanvel.com.br/produtos/15/114255-15.jpg?ims=480x",
      "price": "6,99"
    }
  }
];