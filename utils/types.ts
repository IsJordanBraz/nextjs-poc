export interface IProduct {
  id: number,
  name: string,
  image: string,
  price: string,
}

export interface IPedidos {
  id: number,
  product: IProduct,
}
