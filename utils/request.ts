import axios from "axios";
import { IPedidos, IProduct } from "./types";
import { ProductsMock, PedidosMock } from '../utils/Mock';

export async function getProducts() {
  try {
    const produtos = await axios.request<IProduct[]>({
      url: 'http://localhost:3000/api/v1/products',
      method: "GET",
      auth: {
        username: 'jordan',
        password: 'jordan'
      },
    })
    return produtos.data
  } catch(err) {
    return ProductsMock;
  }
}

export async function getPedidos() {
  try {
    const pedidos =  await axios.request<IPedidos[]>({
      url: 'http://localhost:8000/api/v1/pedidos',
      method: "GET",
      auth: {
        username: 'jordan',
        password: 'jordan'
      },
    })
    return pedidos.data
  } catch(err) {
    return PedidosMock;
  }  
}

