import Image from 'next/image'

import Styles from '../styles/product.module.css'
import axios from 'axios';
import { IProduct } from '../utils/types';

const Product = ({product}: { product: IProduct }) => {

  const handleSubmit = async e => {
    e.preventDefault();

    try {
      await axios.post('http://localhost:3000/api/v1/pedidos', {
        "id": product.id,
        "name": product.name,
        "image": product.image,
        "price": product.price        
      }, {
        auth: {
          username: 'jordan',
          password: 'jordan'
        }
      });
    } catch (error) {
      console.log(error)
    }
  };
  return (
    <div className={Styles.product} onClick={handleSubmit}>
      <Image
        src={product.image}
        alt="image of product"
        width={200}
        height={200}
      />
      <span className={Styles.productName}>{product.name}</span>
      <span className={Styles.productPrice}>R$ {product.price}</span>
      
    </div>
  )
}

export default Product;