import Link from 'next/link'
import Styles from '../styles/header.module.css'

const Header = () => (
  <>
    <header className={Styles.header}>
      <Link href="/">
        <a className={Styles.title}>Home</a>
      </Link>
      <Link href="/produtos">
        <a className={Styles.title}>Produtos</a>
      </Link>
      <Link href="/pedidos">
        <a className={Styles.title}>Pedidos</a>
      </Link>    
    </header>
  </>
)

export default Header;