import Styles from '../styles/pedido.module.css'

import Link from 'next/link'
import { IProduct } from '../utils/types';

const Order = ({ order }: { order: IProduct }) => {
  return (
    <div className={Styles.container}>
      <div className={Styles.block}>
        <span className={Styles.date}>22/07/2020</span>
        <span className={Styles.id}>{order.id}</span>
      </div>
      <div className={Styles.block}>
        <span className={Styles.textTitle}>TIPO DE ENTREGA</span>
        <span className={Styles.textBody}>PROGRAMADA</span>
        <span className={Styles.textTitle}>ORIGEM</span>
        <span className={Styles.textBody}>PEDIDO REALIZADO PELA INTERNET</span>
      </div>
      <div className={Styles.block}>
        <span className={Styles.textTitle1}>Status do Pedido</span>
        <span className={Styles.textBody1}>PEDIDO ENTREGUE</span>
      </div>
      <div className={Styles.block}>
        <span className={Styles.textTitle1}>Valor Total</span>
        <span className={Styles.price}>R$ {order.price}</span>
      </div>
      <div className={Styles.block}>
        <Link href="/">
          <a className={Styles.button}>
            <span>DETALHE DO PEDIDO</span>
          </a>
        </Link>
      </div>
    </div>
  )
}

export default Order;
