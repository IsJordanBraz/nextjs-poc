import Styles from '../styles/pedidoList.module.css'
import { IPedidos } from '../utils/types';
import Order from './Order';

const OrderList = ({ ordersList }: { ordersList: IPedidos[] }) => {
  return (
    <div className={Styles.pedidosContainer}>
      {ordersList.map(product => (
        <Order key={product.id} order={product.product}/> 
      ))}
    </div>
  )
}

export default OrderList;
