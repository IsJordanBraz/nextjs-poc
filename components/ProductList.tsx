import Product from "./Product"
import { IProduct } from '../utils/types';

import Styles from '../styles/productList.module.css'
const ProductList = ({ products }: { products: IProduct[] }) => {
  return (
    <div className={Styles.container}>
      { products.map(product => (
        <Product key={product.id} product={product}/>
      ))}      
    </div>
  )
}

export default ProductList;