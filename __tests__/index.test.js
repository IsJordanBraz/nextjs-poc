import { render, screen } from "@testing-library/react";
import Home from '../pages/index'

describe("Home", () => {
  it("Find index text inside page", () => {
    render(<Home />);
    expect(
      screen.getByText("index")
    ).toBeInTheDocument();
  })
})